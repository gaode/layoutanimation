/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.gaode.jla.javax;

import java.awt.Component;
import java.awt.Insets;


class GridBagConstraints implements Cloneable, java.io.Serializable {

    public int gridx;
    public int gridy;
    public int gridwidth;
    public int gridheight;
    public double weightx;
    public double weighty;
    public int anchor;
    public int fill;
    public Insets insets;
    public int ipadx;
    public int ipady;
    int tempX;
    int tempY;
    int tempWidth;
    int tempHeight;
    int minWidth;
    int minHeight;
    transient int ascent;
    transient int descent;
    transient Component.BaselineResizeBehavior baselineResizeBehavior;
    transient int centerPadding;
    transient int centerOffset;
	
	GridBagConstraints(java.awt.GridBagConstraints c) {
		this.anchor = c.anchor;
		this.fill = c.fill;
		this.gridheight = c.gridheight;
		this.gridwidth = c.gridwidth;
		this.gridx = c.gridx;
		this.gridy = c.gridy;
		this.insets = c.insets;
		this.ipadx = c.ipadx;
		this.ipady = c.ipady;
	}
	
	public java.awt.GridBagConstraints copy() {
		java.awt.GridBagConstraints c = new java.awt.GridBagConstraints();
		c.anchor = this.anchor;
		c.fill = this.fill;
		c.gridheight = this.gridheight;
		c.gridwidth = this.gridwidth;
		c.gridx = this.gridx;
		c.gridy = this.gridy;
		c.insets = this.insets;
		c.ipadx = this.ipadx;
		c.ipady = this.ipady;
		c.weightx = this.weightx;
		c.weighty = this.weighty;
		return c;
	}

    public Object clone () {
        try {
            GridBagConstraints c = (GridBagConstraints)super.clone();
            c.insets = (Insets)insets.clone();
            return c;
        } catch (CloneNotSupportedException e) {
            // this shouldn't happen, since we are Cloneable
            throw new InternalError();
        }
    }

    boolean isVerticallyResizable() {
        return (fill == java.awt.GridBagConstraints.BOTH || fill == java.awt.GridBagConstraints.VERTICAL);
    }
}