package me.gaode.jla;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import me.gaode.jla.animator.AnimatedSupport;

/**
 *
 * @author DGao
 */
public class FreeLayout extends AnimatedSupport implements LayoutManager2 {

    private Map<Component, Rectangle> constraints = new HashMap<Component, Rectangle>();
	
	public FreeLayout() {
		this.skipResize = false;
	}

    @Override
    public void addLayoutComponent(Component comp, Object constraints) {
        if (constraints instanceof Rectangle) {
            this.constraints.put(comp, (Rectangle) constraints);
        } else {
            throw new IllegalArgumentException("Constraint must be a java.awt.Rectangle instance");
        }
    }

    @Override
    public Dimension maximumLayoutSize(Container target) {
        return new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
    }

    @Override
    public float getLayoutAlignmentX(Container target) {
        return 0.5f;
    }

    @Override
    public float getLayoutAlignmentY(Container target) {
        return 0.5f;
    }

    @Override
    public void invalidateLayout(Container target) {
    }

    @Override
    public void addLayoutComponent(String name, Component comp) {
    }

    @Override
    public void removeLayoutComponent(Component comp) {
        super.removeComponent(comp);
        this.constraints.remove(comp);
    }

    @Override
    public Dimension preferredLayoutSize(Container parent) {
        synchronized (parent.getTreeLock()) {
            Dimension d = new Dimension(0, 0);
            for (Rectangle rect : this.constraints.values()) {
                d.width = Math.max(d.width, rect.x + rect.width);
                d.height = Math.max(d.height, rect.y + rect.height);
            }
            return d;
        }
    }

    @Override
    public Dimension minimumLayoutSize(Container parent) {
        return preferredLayoutSize(parent);
    }

    @Override
    public void layoutContainer(Container parent) {
        synchronized (parent.getTreeLock()) {
            super.startAnimation(parent);
            for (Entry<Component, Rectangle> e : this.constraints.entrySet()) {
                if (e.getKey().isVisible()) {
                    super.setBounds(e.getKey(), e.getValue().x, e.getValue().y, e.getValue().width, e.getValue().height);
                }
            }
        }
    }
}
