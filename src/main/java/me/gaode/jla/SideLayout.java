package me.gaode.jla;

import java.awt.*;
import me.gaode.jla.animator.AnimatedSupport;

/**
 *
 * @author DGao
 */
public class SideLayout extends AnimatedSupport implements LayoutManager2 {

    private boolean autoFitSide = true;
    private boolean autoFitCenter = true;
    private static final String INVALID_CONSTRAINT = "Invalid constraint name. Must be TOP, LEFT, BOTTOM, RIGHT, CENTER";
    private Component[] children = new Component[5];//TOP, LEFT, BOTTOM, RIGHT, CENTER
    private int toShow;
    private int shown;
    private String style;

    public SideLayout(String style) {
        this.style = style;
        this.toShow = -1;
        this.shown = -1;
        this.setSkipResize();
    }

    public SideLayout() {
        this(OVERLAP);
    }

    public void setStyle(String style) {
        this.style = style;
        this.setSkipResize();
    }

    public void setAutoFitCenter(boolean autoFitCenter) {
        this.autoFitCenter = autoFitCenter;
        this.setSkipResize();
    }

    public void setAutoFitSide(boolean autoFitSide) {
        this.autoFitSide = autoFitSide;
    }

    @Override
    public void addLayoutComponent(Component comp, Object constraints) {
        final Container parent = comp.getParent();
        if (parent.getLayout() != this) {
            throw new IllegalArgumentException("Component don't belong to this layout manager");
        }
        int index;
        if (constraints instanceof String) {
            try {
                index = getIndex(constraints);
            } catch (IllegalArgumentException e) {
                throw new IllegalArgumentException(INVALID_CONSTRAINT);
            }
        } else {
            throw new IllegalArgumentException(INVALID_CONSTRAINT);
        }

        Component old = children[index];
        children[index] = comp;

        if (children[4] != null) {
            for (int i = 0; i < 4; i++) {
                if (parent.getComponentZOrder(children[i]) > parent.getComponentZOrder(children[4])) {
                    children[index] = old;
                    throw new IllegalArgumentException("Side panel must above the center panel");
                }
            }
        }
    }

    @Override
    public Dimension maximumLayoutSize(Container target) {
        return new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
    }

    @Override
    public float getLayoutAlignmentX(Container target) {
        return 0.5f;
    }

    @Override
    public float getLayoutAlignmentY(Container target) {
        return 0.5f;
    }

    @Override
    public void invalidateLayout(Container target) {
    }

    @Override
    public void addLayoutComponent(String name, Component comp) {
        addLayoutComponent(comp, name);
    }

    @Override
    public void removeLayoutComponent(Component comp) {
        for (int i = 0; i < 5; i++) {
            if (children[i] == comp) {
                children[i] = null;
                super.removeComponent(comp);
                return;
            }
        }
    }

    @Override
    public Dimension preferredLayoutSize(Container parent) {
        synchronized (parent.getTreeLock()) {
            Dimension dim = new Dimension(0, 0);
            for (int i = 0; i < 5; i++) {
                if (children[i] == null) {
                    continue;
                } else {
                    final Dimension preferredSize = children[i].getPreferredSize();
                    dim.width = Math.max(dim.width, preferredSize.width);
                    dim.height = Math.max(dim.height, preferredSize.height);
                }
            }
            final Insets insets = parent.getInsets();
            dim.width += insets.left + insets.right;
            dim.height += insets.top + insets.bottom;
            return dim;
        }
    }

    @Override
    public Dimension minimumLayoutSize(Container parent) {
        synchronized (parent.getTreeLock()) {
            Dimension dim = new Dimension(0, 0);
            for (int i = 0; i < 5; i++) {
                if (children[i] == null) {
                    continue;
                } else {
                    final Dimension preferredSize = children[i].getMinimumSize();
                    dim.width = Math.max(dim.width, preferredSize.width);
                    dim.height = Math.max(dim.height, preferredSize.height);
                }
            }
            final Insets insets = parent.getInsets();
            dim.width += insets.left + insets.right;
            dim.height += insets.top + insets.bottom;
            return dim;
        }
    }

    public void toggle(String constraint, Container parent) {
        int index = getIndex(constraint);
        if (this.toShow == index) {
            this.toShow = -2;
        } else {
            this.toShow = index;
        }
        parent.doLayout();
        parent.repaint();
    }

    private int getIndex(Object constraint) throws IllegalArgumentException {
        if (constraint == null) {
            return 4;
        }
        String c = String.valueOf(constraint).trim();
        if (TOP.equalsIgnoreCase(c)) {
            return 0;
        } else if (LEFT.equalsIgnoreCase(c)) {
            return 1;
        } else if (BOTTOM.equalsIgnoreCase(c)) {
            return 2;
        } else if (RIGHT.equalsIgnoreCase(c)) {
            return 3;
        } else if (CENTER.equalsIgnoreCase(c)) {
            return 4;
        } else {
            throw new IllegalArgumentException(INVALID_CONSTRAINT);
        }
    }

    public void show(String constraint, Container parent) {
        int index = getIndex(constraint);
        this.toShow = index;
        parent.doLayout();
        parent.repaint();
    }

    public void hide(Container parent) {
        this.toShow = -2;
        parent.doLayout();
        parent.repaint();
    }

    @Override
    public void layoutContainer(Container parent) {
        synchronized (parent.getTreeLock()) {
            super.startAnimation(parent);
            if (toShow != -1 || animator.isInAnimation()) {
                super.startAnimation(parent);
                for (int i = 0; i < 4; i++) {
                    if (i == toShow) {
                        show(parent, i);
                    } else {
                        hide(parent, i);
                    }
                }
                showCenter(parent, toShow, true);
                if (toShow > -1) {
                    shown = toShow;
                }
            } else {
                reset(parent);
            }
            toShow = -1;
        }
    }

    private void show(Container parent, int index) {
        Component child = children[index];
        if (child == null) {
            return;
        }
        if (!child.isVisible()) {
            child.setVisible(true);
        }
        Rectangle r = getInnerBounds(index, parent);
        super.setBounds(child, r.x, r.y, r.width, r.height);
    }

    private void hide(Container parent, int index) {
        Component child = children[index];
        if (child == null) {
            return;
        }
        Rectangle r = getOuterBounds(index, parent);
        super.setBounds(child, r.x, r.y, r.width, r.height);
    }

    private void showCenter(Container parent, int index, boolean animated) {
        final Component center = children[4];
        if (center == null || !center.isVisible()) {
            return;
        }
        final Insets insets = parent.getInsets();
        final int netWidth = parent.getSize().width - insets.left - insets.right;
        final int netHeight = parent.getSize().height - insets.top - insets.bottom;
        if (index < 0 || index > 3 || children[index] == null) {
            if (animated) {
                super.setBounds(center, insets.left, insets.top, netWidth, netHeight);
            } else {
                center.setBounds(insets.left, insets.top, netWidth, netHeight);
            }
            return;
        }
        final Component child = children[index];
        final Dimension childSize = child.getPreferredSize();
        int width = netWidth;
        int height = netHeight;
        int x = insets.left;
        int y = insets.top;
        if (PUSH.equalsIgnoreCase(style)) {
            switch (index) {
                case 0:
                    y += childSize.height;
                    break;
                case 1:
                    x += childSize.width;
                    break;
            }
            if (autoFitCenter) {
                switch (index) {
                    case 0:
                    case 2:
                        height = netHeight - childSize.height;
                        break;
                    case 1:
                    case 3:
                        width = netWidth - childSize.width;
                        break;
                }
            } else {
                switch (index) {
                    case 2:
                        y = -insets.bottom - childSize.height;
                        break;
                    case 3:
                        x = -insets.right - childSize.width;
                        break;
                }
            }
            if (animated) {
                super.setBounds(center, x, y, width, height);
            } else {
                center.setBounds(x, y, width, height);
            }
        } else {
            if (animated) {
                super.setBounds(center, insets.left, insets.top, netWidth, netHeight);
            } else {
                center.setBounds(insets.left, insets.top, netWidth, netHeight);
            }
        }
    }

    private void reset(Container parent) {
        for (int i = 0; i < 4; i++) {
            final Component child = children[i];
            if (child != null) {
                if (shown == i) {
                    child.setBounds(getInnerBounds(i, parent));
                } else {
                    child.setBounds(getOuterBounds(i, parent));
                }
            }
        }
        if (children[4] != null) {
            showCenter(parent, shown, false);
        }
    }

    private Rectangle getOuterBounds(int index, Container parent) {
        final Insets insets = parent.getInsets();
        final Dimension preferredSize = children[index].getPreferredSize();
        final int netParentWidth = parent.getWidth() - insets.left - insets.right;
        final int netParentHeight = parent.getHeight() - insets.top - insets.bottom;
        int x = 0;
        int y = 0;
        int width = 0;
        int height = 0;
        if (autoFitSide) {
            switch (index) {
                case 0:
                case 2:
                    width = netParentWidth;
                    height = preferredSize.height;
                    break;
                case 1:
                case 3:
                    width = preferredSize.width;
                    height = netParentHeight;
                    break;
            }
            switch (index) {
                case 0:
                    x = insets.left;
                    y = -preferredSize.height;
                    break;
                case 1:
                    x = -preferredSize.width;
                    y = insets.top;
                    break;
                case 2:
                    x = insets.left;
                    y = parent.getHeight();
                    break;
                case 3:
                    x = parent.getWidth();
                    y = insets.top;
                    break;
            }
        } else {
            width = preferredSize.width;
            height = preferredSize.height;
            switch (index) {
                case 0:
                    x = (netParentWidth - preferredSize.width) / 2 + insets.left;
                    y = -preferredSize.height;
                    break;
                case 2:
                    x = (netParentWidth - preferredSize.width) / 2 + insets.left;
                    y = parent.getHeight();
                    break;
                case 1:
                    x = -preferredSize.width;
                    y = (netParentHeight - preferredSize.height) / 2 + insets.top;
                    break;
                case 3:
                    x = parent.getWidth();
                    y = (netParentHeight - preferredSize.height) / 2 + insets.top;
                    break;
            }
        }
        Rectangle r = new Rectangle(x, y, width, height);
        return r;
    }

    private Rectangle getInnerBounds(int index, Container parent) {
        final Insets insets = parent.getInsets();
        final Dimension preferredSize = children[index].getPreferredSize();
        final int netParentWidth = parent.getWidth() - insets.left - insets.right;
        final int netParentHeight = parent.getHeight() - insets.top - insets.bottom;
        int x = 0;
        int y = 0;
        int width = 0;
        int height = 0;
        if (autoFitSide) {
            switch (index) {
                case 0:
                case 2:
                    width = netParentWidth;
                    height = preferredSize.height;
                    break;
                case 1:
                case 3:
                    width = preferredSize.width;
                    height = netParentHeight;
                    break;
            }
            switch (index) {
                case 0:
                case 1:
                    x = insets.left;
                    y = insets.top;
                    break;
                case 2:
                    x = insets.left;
                    y = netParentHeight - insets.bottom - preferredSize.height;
                    break;
                case 3:
                    x = netParentWidth - insets.right - preferredSize.width;
                    y = insets.top;
                    break;
            }
        } else {
            width = preferredSize.width;
            height = preferredSize.height;
            switch (index) {
                case 0:
                    x = (netParentWidth - preferredSize.width) / 2 + insets.left;
                    y = insets.top;
                    break;
                case 2:
                    x = (netParentWidth - preferredSize.width) / 2 + insets.left;
                    y = netParentHeight - preferredSize.height - insets.bottom;
                    break;
                case 1:
                    x = insets.left;
                    y = (netParentHeight - preferredSize.height) / 2 + insets.top;
                    break;
                case 3:
                    x = netParentWidth - preferredSize.width - insets.right;
                    y = (netParentHeight - preferredSize.height) / 2 + insets.top;
                    break;
            }
        }
        Rectangle r = new Rectangle(x, y, width, height);
        return r;
    }

    private void setSkipResize() {
        this.skipResize = OVERLAP.equals(style) || !autoFitCenter;
    }
}
