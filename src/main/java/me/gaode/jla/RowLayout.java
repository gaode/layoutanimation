package me.gaode.jla;

import me.gaode.jla.animator.AnimatedSupport;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * fixed row height even there is no element in the row
 * negative row height are flexible
 *
 * @author DGao
 */
public class RowLayout extends AnimatedSupport implements LayoutManager2 {
    
    private int[] rowHeight;
    private int hgap;
    private int vgap;
	private int grid;
    private List<Component>[] rows;
    
    public RowLayout(int... rowHeight) {
        this.rowHeight = rowHeight;
        this.rows = new List[rowHeight.length];
		this.skipResize = false;
    }
    
    public RowLayout(int hgap, int vgap, int[] columnWidth) {
        this(columnWidth);
        this.hgap = hgap;
        this.vgap = vgap;
    }
    
    public RowLayout(int hgap, int vgap, int grid, int[] columnWidth) {
        this(hgap, vgap, columnWidth);
		this.grid = grid;
    }
    
    public void setHGap(int hgap) {
        this.hgap = hgap;
    }
    
    public void setVGap(int vgap) {
        this.vgap = vgap;
    }
	
	public void setGrid(int grid) {
		this.grid = grid;
	}

	@Override
	public void addLayoutComponent(Component comp, Object constraints) {
		try{
            final Integer index = Integer.valueOf(String.valueOf(constraints));
            if(index < 0 || index > rows.length - 1) {
                throw new IllegalArgumentException("No such row " + constraints);
            }
            List<Component> row = rows[index];
            if(row == null) {
                row = new ArrayList<Component>();
                rows[index] = row;
            }
            row.add(comp);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("constraint must be a number String. E.g. '3'");
        }
	}

	@Override
	public Dimension maximumLayoutSize(Container target) {
		return new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
	}

	@Override
	public float getLayoutAlignmentX(Container target) {
		return 0.5f;
	}

	@Override
	public float getLayoutAlignmentY(Container target) {
		return 0.5f;
	}

	@Override
	public void invalidateLayout(Container target) {
	}

	@Override
	public void addLayoutComponent(String name, Component comp) {
        addLayoutComponent(comp, name);
	}

	@Override
	public void removeLayoutComponent(Component comp) {
        for(List<Component> column : rows) {
            if(column != null && column.remove(comp)) {
				super.removeComponent(comp);
                return;
            }
        }
	}

	@Override
	public Dimension preferredLayoutSize(Container parent) {
        synchronized(parent.getTreeLock()) {
            return getSize(parent, new ColumnLayout.Tile(){

                @Override
                public int getD1(Component comp) {
                    return comp.getPreferredSize().height;
                }

                @Override
                public int getD2(Component comp) {
                    return comp.getPreferredSize().width;
                }
            });
        }
	}

	@Override
	public Dimension minimumLayoutSize(Container parent) {
        synchronized(parent.getTreeLock()) {
            return getSize(parent, new ColumnLayout.Tile(){

                @Override
                public int getD1(Component comp) {
                    return comp.getMinimumSize().height;
                }

                @Override
                public int getD2(Component comp) {
                    return comp.getMinimumSize().width;
                }
            });
        }
	}

	@Override
	public void layoutContainer(Container parent) {
        synchronized(parent.getTreeLock()) {
            super.startAnimation(parent);
            final Insets insets = parent.getInsets();
            final int netParentHeight = parent.getSize().height - insets.top - insets.bottom;
            int[] heights = new int[rowHeight.length];
            System.arraycopy(this.rowHeight, 0, heights, 0, heights.length);
            int pSum = 0;
            int nSum = 0;
            for(int height : heights) {
                if(height > 0) {
                    pSum += height;
                } else {
                    nSum += height;
                }
                pSum += vgap;
            }
            if(pSum > 0) {
                pSum -= vgap;
            }
            if(nSum != 0) {
                for(int i=0;i<heights.length;i++) {
                    if(heights[i] < 0) {
                        heights[i] = (int)(1.0 * (netParentHeight - pSum) * heights[i] / nSum);
                        if(heights[i] < 0){
                            heights[i] = 0;
                        }
                    }
                }
            }
            int y = insets.top;
            boolean firstRow = true;
            for(int i=0;i<heights.length;i++) {
                if(firstRow) {
                    firstRow = false;
                } else {
                    y += vgap;
                }
                List<Component> row = rows[i];
                if(row != null && heights[i] > 0) {
                    int x = insets.left;
                    boolean firstComponent = true;
                    for(Component comp : rows[i]) {
                        if(!comp.isVisible()) {
                            continue;
                        }
                        if(firstComponent) {
                            firstComponent = false;
                        } else {
							x += hgap;
							if(grid > 0) {
								x += grid - (x - insets.left) % grid;
							}
                        }
                        final int width = comp.getPreferredSize().width;
                        super.setBounds(comp, x, y, width, heights[i]);
                        x += width;
                    }
                }
                y += heights[i];
            }
        }
	}
    
    private Dimension getSize(Container parent, final ColumnLayout.Tile tile) {
        final int[] size = ColumnLayout.getSize(parent, rowHeight, rows, vgap, hgap, grid, tile);
        Dimension dim = new Dimension(size[1], size[0]);
        final Insets insets = parent.getInsets();
        dim.width += insets.left + insets.right;
        dim.height += insets.top + insets.bottom;
        return dim;
    }

}
