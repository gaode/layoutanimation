/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.gaode.jla;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import me.gaode.jla.animator.AnimatedSupport;

/**
 * Layout components in tiles. Each tile can have a unit tile size or size of
 * several merged unit tile.
 *
 * @author gaode
 */
public class TileLayout extends AnimatedSupport implements LayoutManager2 {

	private int tileWidth;
	private int tileHeight;
	private int hgap;
	private int vgap;
	private Map<Component, Tile> tiles = new HashMap<Component, Tile>();

	public TileLayout(int tileWidth, int tileHeight) {
		this.tileWidth = tileWidth;
		this.tileHeight = tileHeight;
		this.skipResize = true;
	}

	public TileLayout(int tileWidth, int tileHeight, int hgap, int vgap) {
		this(tileWidth, tileHeight);
		this.hgap = hgap;
		this.vgap = vgap;
	}

	public void setHGap(int hgap) {
		this.hgap = hgap;
	}

	public void setVGap(int vgap) {
		this.vgap = vgap;
	}

	public void setTileHeight(int tileHeight) {
		this.tileHeight = tileHeight;
	}

	public void setTileWidth(int tileWidth) {
		this.tileWidth = tileWidth;
	}

	public static class Tile implements Cloneable {

		public int rowSpan = 1;
		public int colSpan = 1;
		private boolean isPainted = false;
		private Component comp;

		@Override
		public Tile clone() {
			Tile tile = new Tile();
			tile.rowSpan = rowSpan;
			tile.colSpan = colSpan;
			tile.isPainted = isPainted;
			return tile;
		}
	}

	@Override
	public void addLayoutComponent(Component comp, Object constraints) {
		if (comp.getParent().getLayout() != this) {
			throw new IllegalArgumentException("Component not managed by this layout manager");
		}
		final Tile tile;
		if (constraints == null) {
			tile = new Tile();
		} else if (constraints instanceof Tile) {
			tile = ((Tile) constraints).clone();
		} else {
			throw new IllegalArgumentException("Invalid constraints, must be a instance of Tile");
		}
		tile.comp = comp;
		tiles.put(comp, tile);
	}

	@Override
	public Dimension maximumLayoutSize(Container target) {
		return new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
	}

	@Override
	public float getLayoutAlignmentX(Container target) {
		return 0.5f;
	}

	@Override
	public float getLayoutAlignmentY(Container target) {
		return 0.5f;
	}

	@Override
	public void invalidateLayout(Container target) {
	}

	@Override
	public void addLayoutComponent(String name, Component comp) {
	}

	@Override
	public void removeLayoutComponent(Component comp) {
		super.removeComponent(comp);
		tiles.remove(comp);
	}

	@Override
	public Dimension preferredLayoutSize(Container parent) {
		synchronized (parent.getTreeLock()) { 
			final Insets insets = parent.getInsets();
			int netWidth = parent.getWidth() - insets.left - insets.right;
			int colSize = (netWidth + hgap) / (tileWidth + hgap);
			if (colSize == 0) {
				return new Dimension(0, 0);
			}
			final List<Tile[]> ts = layoutTiles(colSize, parent.getComponents());
			return new Dimension(colSize * (tileWidth + hgap) - hgap, ts.size() * (tileHeight + vgap) - vgap);
		}
	}

	@Override
	public Dimension minimumLayoutSize(Container parent) {
		synchronized (parent.getTreeLock()) {
			final Insets insets = parent.getInsets();
			return new Dimension(insets.left + insets.right, insets.top + insets.bottom);
		}
	}

	@Override
	public void layoutContainer(Container parent) {
		synchronized (parent.getTreeLock()) {
			super.startAnimation(parent);
			final Insets insets = parent.getInsets();
			int netWidth = parent.getWidth() - insets.left - insets.right;
			int colSize = (netWidth + hgap) / (tileWidth + hgap);
			if (colSize == 0) {
				return;
			}
			final List<Tile[]> ts = layoutTiles(colSize, parent.getComponents());
			for(Tile tile : tiles.values()) {
				tile.isPainted = false;
			}
			for (int i = 0; i < ts.size(); i++) {
				final Tile[] row = ts.get(i);
				for (int j = 0; j < row.length; j++) {
					Tile tile = row[j];
					if(tile == null) {
						continue;
					}
					if(!tile.isPainted) {
						int x = j * (tileWidth + hgap) + insets.left;
						int y = i * (tileHeight + vgap) + insets.top;
						int width = tile.colSpan * (tileWidth +  hgap) - hgap;
						int height = tile.rowSpan * (tileHeight + vgap) - vgap;
						super.setBounds(tile.comp, x, y, width, height);
						tile.isPainted = true;
					}
				}
			}
			for(Tile tile : tiles.values()) {
				if(!tile.isPainted) {
                    super.setBounds(tile.comp, 0, 0, 0, 0);
                }
			}
		}
	}

	private List<Tile[]> layoutTiles(int colSize, Component[] components) {
		List<Tile[]> ts = new ArrayList<Tile[]>();
		for (Component comp : components) {
			if (!comp.isVisible()) {
				continue;
			}
			Tile tile = tiles.get(comp);
			if (tile == null) {
				continue;
			}
            if(!tile.comp.isVisible()) {
                continue;
            }
			if (tile.colSpan > colSize) {
				continue;
			}
			int x, y = 0;
			while ((x = testSpace(tile, ts, y, colSize)) == -1) {
				y++;
			}
			fill(tile, ts, x, y);
		}
		for (int j = ts.size() - 1; j > -1; j--) {
			boolean isEmpty = true;
			final Tile[] row = ts.get(j);
			for (int i = 0; i < colSize; i++) {
				if (row[i] != null) {
					isEmpty = false;
					break;
				}
			}
			if (isEmpty) {
				ts.remove(j);
			} else {
				return ts;
			}
		}
		return ts;
	}

	private int testSpace(Tile tile, List<Tile[]> ts, int y, int colSize) {
		for (int i = ts.size(); i < y + tile.rowSpan; i++) {
			ts.add(new Tile[colSize]);
		}
		for (int i = 0; i < colSize - tile.colSpan + 1; i++) {
			if (canFill(tile, ts, i, y)) {
				return i;
			}
		}
		return -1;
	}

	private boolean canFill(Tile tile, List<Tile[]> ts, int x, int y) {
		for (int i = y; i < y + tile.rowSpan; i++) {
			final Tile[] row = ts.get(i);
			for (int j = x; j < x + tile.colSpan; j++) {
				if (row[j] != null) {
					return false;
				}
			}
		}
		return true;
	}

	private void fill(Tile tile, List<Tile[]> ts, int x, int y) {
		for (int i = y; i < y + tile.rowSpan; i++) {
			final Tile[] row = ts.get(i);
			for (int j = x; j < x + tile.colSpan; j++) {
				row[j] = tile;
			}
		}
	}
}
