package me.gaode.jla;

import java.awt.*;
import me.gaode.jla.animator.AnimatedSupport;

/**
 *
 * @author DGao
 */
public class SlideLayout extends AnimatedSupport implements LayoutManager2 {

	private int current = 0;
	private String direction;
	private String style;

	public SlideLayout() {
		this(PUSH, RIGHT);
	}

	public SlideLayout(String style, String direction) {
		this.style = style;
		this.direction = direction;
		this.skipResize = true;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	@Override
	public void addLayoutComponent(Component comp, Object constraints) {
		addLayoutComponent(comp);
	}

	@Override
	public Dimension maximumLayoutSize(Container target) {
		return new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
	}

	@Override
	public float getLayoutAlignmentX(Container target) {
		return 0.5f;
	}

	@Override
	public float getLayoutAlignmentY(Container target) {
		return 0.5f;
	}

	@Override
	public void invalidateLayout(Container target) {
	}

	@Override
	public void addLayoutComponent(String name, Component comp) {
		addLayoutComponent(comp);
	}

	private void addLayoutComponent(Component comp) {
		synchronized (comp.getTreeLock()) {
			final Container parent = comp.getParent();
			checkLayout(parent);
			final int zOrder = parent.getComponentZOrder(comp);
			if (zOrder < current) {
				comp.setBounds(getBoundsAbove(comp));
			} else if (zOrder == current) {
				comp.setBounds(getBoundsMiddle(comp));
			} else {
				comp.setBounds(getBoundsUnder(comp));
			}
		}
	}

	@Override
	public void removeLayoutComponent(Component comp) {
		synchronized (comp.getTreeLock()) {
			final Container parent = comp.getParent();
			checkLayout(parent);
			final int zOrder = parent.getComponentZOrder(comp);
			if (zOrder == current) {
				current++;
			}
			if (current >= parent.getComponentCount()) {
				current = parent.getComponentCount() - 1;
			}
		}
	}

	@Override
	public Dimension preferredLayoutSize(Container parent) {
		synchronized (parent.getTreeLock()) {
			checkLayout(parent);
			Insets insets = parent.getInsets();
			int ncomponents = parent.getComponentCount();
			int w = 0;
			int h = 0;

			for (int i = 0; i < ncomponents; i++) {
				Component comp = parent.getComponent(i);
				Dimension d = comp.getPreferredSize();
				if (d.width > w) {
					w = d.width;
				}
				if (d.height > h) {
					h = d.height;
				}
			}
			return new Dimension(insets.left + insets.right + w,
					insets.top + insets.bottom + h);
		}
	}

	@Override
	public Dimension minimumLayoutSize(Container parent) {
		synchronized (parent.getTreeLock()) {
			checkLayout(parent);
			Insets insets = parent.getInsets();
			int ncomponents = parent.getComponentCount();
			int w = 0;
			int h = 0;

			for (int i = 0; i < ncomponents; i++) {
				Component comp = parent.getComponent(i);
				Dimension d = comp.getMinimumSize();
				if (d.width > w) {
					w = d.width;
				}
				if (d.height > h) {
					h = d.height;
				}
			}
			return new Dimension(insets.left + insets.right + w,
					insets.top + insets.bottom + h);
		}
	}

	@Override
	public void layoutContainer(Container parent) {
		synchronized (parent.getTreeLock()) {
			if (parent.getComponentCount() == 0) {
				return;
			}
			if (current == -1) {
				current = 0;
			}
			checkLayout(parent);
            
            boolean topPanelFound = false;

			for (int i = 0; i < parent.getComponentCount(); i++) {
				final Component comp = parent.getComponent(i);
				final Rectangle dest = animator == null ? null : animator.getDestination(comp);
				final int zOrder = parent.getComponentZOrder(comp);
				final Rectangle r;
				if (zOrder < current) {
					r = getBoundsAbove(comp);
                    comp.setVisible(dest != null || zOrder == current - 1);
				} else if (zOrder == current) {
					r = getBoundsMiddle(comp);
                    comp.setVisible(true);
				} else {
					r = getBoundsUnder(comp);
                    if(dest == null && !topPanelFound) {
                        comp.setVisible(true);
                        topPanelFound = true;
                    } else {
                        if(topPanelFound) {
                            comp.setVisible(false);
                        }
                    }
				}
				if(dest == null) {
					super.setBounds(comp, r.x, r.y, r.width, r.height);
				} else {
					dest.setBounds(r);
				}
			}
		}
	}

	void checkLayout(Container parent) {
		if (parent.getLayout() != this) {
			throw new IllegalArgumentException("wrong parent for SlideLayout");
		}
	}

	public void first(Container parent) {
		synchronized (parent.getTreeLock()) {
			checkLayout(parent);
			int count = parent.getComponentCount();
			if (count > 0) {
				current = 0;
				arrangeComponents(parent);
			} else {
				current = -1;
			}
		}
	}

	public void next(Container parent) {
		synchronized (parent.getTreeLock()) {
			checkLayout(parent);
			final int size = parent.getComponentCount();
			if (size > 0) {
				current++;
				checkBounds(size);
				arrangeComponents(parent);
			} else {
				current = -1;
			}
			parent.doLayout();
            parent.repaint();
		}
	}

	public void show(Container parent, int index) {
		synchronized (parent.getTreeLock()) {
			checkLayout(parent);
			final int size = parent.getComponentCount();
			if (size > 0) {
				current = index;
				checkBounds(size);
				arrangeComponents(parent);
			} else {
				current = -1;
			}
			parent.doLayout();
            parent.repaint();
		}
	}

	public void previous(Container parent) {
		synchronized (parent.getTreeLock()) {
			checkLayout(parent);
			final int size = parent.getComponentCount();
			if (size > 0) {
				current--;
				checkBounds(size);
				arrangeComponents(parent);
			} else {
				current = -1;
			}
			parent.doLayout();
            parent.repaint();
		}
	}

	private void checkBounds(final int size) {
		if (current > size - 1) {
			current = size - 1;
		} else if (current < 0) {
			current = 0;
		}
	}

	public boolean hasNext(Container parent) {
		synchronized (parent.getTreeLock()) {
			checkLayout(parent);
			final int size = parent.getComponentCount();
			return current < size - 1;
		}
	}

	public boolean hasPrevious(Container parent) {
		synchronized (parent.getTreeLock()) {
			checkLayout(parent);
			final int size = parent.getComponentCount();
			return current > 0 && size > 0;
		}
	}

	public boolean hasFirst(Container parent) {
		synchronized (parent.getTreeLock()) {
			checkLayout(parent);
			return parent.getComponentCount() > 0;
		}
	}

	private Rectangle getBoundsAbove(Component comp) {
		final Container parent = comp.getParent();
		final Insets insets = parent.getInsets();
		Rectangle r = new Rectangle();
		r.width = parent.getWidth() - insets.left - insets.right;
		r.height = parent.getHeight() - insets.top - insets.bottom;
		if (TOP.equalsIgnoreCase(direction)) {
			r.x = insets.left;
			r.y = -parent.getHeight() - insets.bottom;
		} else if (LEFT.equalsIgnoreCase(direction)) {
			r.x = -parent.getWidth() - insets.right;
			r.y = insets.top;
		} else if (BOTTOM.equalsIgnoreCase(direction)) {
			r.x = insets.left;
			r.y = parent.getHeight() + insets.top;
		} else {
			r.x = parent.getWidth() + insets.left;
			r.y = insets.top;
		}
		return r;
	}

	private Rectangle getBoundsMiddle(Component comp) {
		final Container parent = comp.getParent();
		final Insets insets = parent.getInsets();
		Rectangle r = new Rectangle();
		r.width = parent.getWidth() - insets.left - insets.right;
		r.height = parent.getHeight() - insets.top - insets.bottom;
		r.x = insets.left;
		r.y = insets.top;
		return r;
	}

	private Rectangle getBoundsUnder(Component comp) {
		if (OVERLAP.equals(style)) {
			return getBoundsMiddle(comp);
		} else {
			final Container parent = comp.getParent();
			final Insets insets = parent.getInsets();
			Rectangle r = new Rectangle();
			r.width = parent.getWidth() - insets.left - insets.right;
			r.height = parent.getHeight() - insets.top - insets.bottom;
			if (TOP.equalsIgnoreCase(direction)) {
				r.x = insets.left;
				r.y = parent.getHeight() + insets.top;
			} else if (LEFT.equalsIgnoreCase(direction)) {
				r.x = parent.getWidth() + insets.left;
				r.y = insets.top;
			} else if (BOTTOM.equalsIgnoreCase(direction)) {
				r.x = insets.left;
				r.y = -parent.getHeight() - insets.bottom;
			} else {
				r.x = -parent.getWidth() - insets.right;
				r.y = insets.top;
			}
			return r;
		}
	}

	private Component getComponentWithZOrder(Container parent, int index) {
		if(index < 0) {
			return null;
		}
		for(Component comp : parent.getComponents()) {
			if(parent.getComponentZOrder(comp) == index) {
				return comp;
			}
		}
		return null;
	}

	private void arrangeComponents(Container parent) {
		super.startAnimation(parent);
		final Component aboveComp = getComponentWithZOrder(parent, current - 1);
		if(aboveComp != null) {
			final Rectangle dest = super.animator.getDestination(aboveComp);
			final Rectangle r = this.getBoundsAbove(aboveComp);
			if(dest == null) {
				super.setBounds(aboveComp, r.x, r.y, r.width, r.height);
			} else {
				dest.setRect(r);
			}
		}
		final Component currentComp = getComponentWithZOrder(parent, current);
		if(currentComp != null) {
			final Rectangle r = this.getBoundsMiddle(currentComp);
			super.setBounds(currentComp, r.x, r.y, r.width, r.height);
		}
		final Component underComp = getComponentWithZOrder(parent, current + 1);
		if(underComp != null) {
			final Rectangle dest = super.animator.getDestination(underComp);
			final Rectangle r = this.getBoundsUnder(underComp);
			if(dest == null) {
				super.setBounds(underComp, r.x, r.y, r.width, r.height);
			} else {
				dest.setBounds(r);
			}
		}
		parent.doLayout();
	}
}
