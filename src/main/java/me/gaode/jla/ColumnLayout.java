/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.gaode.jla;

import me.gaode.jla.animator.AnimatedSupport;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Layout components in fixed/flexible width columns. User must define the column
 * width first. Then user can add child component to any of the column created. 
 * If user specify a positive column width, the column width is fixed. If user 
 * specify a negative column width, the column will fill the rest space of the 
 * container.
 * User can specify the horizontal gap between columns. Or vertical gap between
 * components in each column. User can also specify a negative vertical gap to
 * allow the 'snap to grid' effect in each column. The absolute value of the 
 * vgap is the grid size.
 *
 * @author gaode
 */
public class ColumnLayout extends AnimatedSupport implements LayoutManager2 {

	private int[] columnWidth;
	private int hgap = 0;
	private int vgap = 0;
	private int grid = 0;
	private List<Component>[] columns;

	/**
	 * Create a ColumnLayout with specified column width and zero hgap/vgap
	 * 
	 * @param columnWidth a positive number means a fixed width column, a 
	 * negative number means a flexible width column.
	 */
	public ColumnLayout(int... columnWidth) {
		this.columnWidth = columnWidth;
		this.columns = new List[columnWidth.length];
		this.skipResize = false;
	}

	/**
	 * Create a ColumnLayout with specified column width and hgap/vgap
	 * 
	 * @param hgap horizontal gaps between columns
	 * @param vgap vertical gap between components in each column. 
	 * @param columnWidth a positive number means a fixed width column, a 
	 * negative number means a flexible width column.
	 */
	public ColumnLayout(int hgap, int vgap, int[] columnWidth) {
		this(columnWidth);
		this.hgap = hgap;
		this.vgap = vgap;
	}

	/**
	 * Create a ColumnLayout with specified column width and hgap/vgap
	 * 
	 * @param hgap horizontal gaps between columns
	 * @param vgap vertical gap between components in each column. 
	 * @param grid the grid size if it is bigger than zero.
	 * @param columnWidth a positive number means a fixed width column, a 
	 * negative number means a flexible width column.
	 */
	public ColumnLayout(int hgap, int vgap, int grid, int[] columnWidth) {
		this(hgap, vgap, columnWidth);
		this.grid = grid;
	}

	public void setHGap(int hgap) {
		this.hgap = hgap;
	}

	public void setVGap(int vgap) {
		this.vgap = vgap;
	}

	public void setGrid(int grid) {
		this.grid = grid;
	}

	@Override
	public void addLayoutComponent(Component comp, Object constraints) {
		try {
			if(constraints == null) {
				constraints = "0";
			}
			final Integer index = Integer.valueOf(String.valueOf(constraints));
			if (index < 0 || index > columns.length - 1) {
				throw new IllegalArgumentException("No such column " + constraints);
			}
			List<Component> column = columns[index];
			if (column == null) {
				column = new ArrayList<Component>();
				columns[index] = column;
			}
			column.add(comp);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("constraint must be a number String. E.g. '3'");
		}
	}

	@Override
	public Dimension maximumLayoutSize(Container target) {
		return new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
	}

	@Override
	public float getLayoutAlignmentX(Container target) {
		return 0.5f;
	}

	@Override
	public float getLayoutAlignmentY(Container target) {
		return 0.5f;
	}

	@Override
	public void invalidateLayout(Container target) {
	}

	@Override
	public void addLayoutComponent(String name, Component comp) {
		addLayoutComponent(comp, name);
	}

	@Override
	public void removeLayoutComponent(Component comp) {
		for (List<Component> column : columns) {
			if (column != null && column.remove(comp)) {
				super.removeComponent(comp);
				return;
			}
		}
	}

	@Override
	public Dimension preferredLayoutSize(Container parent) {
		synchronized (parent.getTreeLock()) {
			return getSize(parent, new Tile() {

				@Override
				public int getD1(Component comp) {
					return comp.getPreferredSize().width;
				}

				@Override
				public int getD2(Component comp) {
					return comp.getPreferredSize().height;
				}
			});
		}
	}

	@Override
	public Dimension minimumLayoutSize(Container parent) {
		synchronized (parent.getTreeLock()) {
			return getSize(parent, new Tile() {

				@Override
				public int getD1(Component comp) {
					return comp.getMinimumSize().width;
				}

				@Override
				public int getD2(Component comp) {
					return comp.getMinimumSize().height;
				}
			});
		}
	}

	private Dimension getSize(Container parent, final Tile tile) {
		final Insets insets = parent.getInsets();
		final int[] size = getSize(parent, columnWidth, columns, hgap, vgap, grid, tile);
		Dimension dim = new Dimension(size[0], size[1]);
		dim.width += insets.left + insets.right;
		dim.height += insets.top + insets.bottom;
		return dim;
	}

	@Override
	public void layoutContainer(Container parent) {
		synchronized (parent.getTreeLock()) {
			super.startAnimation(parent);
			final Insets insets = parent.getInsets();
			final int netParentWidth = parent.getSize().width - insets.left - insets.right;
			int[] widths = new int[columnWidth.length];
			System.arraycopy(this.columnWidth, 0, widths, 0, widths.length);
			int pSum = 0;
			int nSum = 0;
			for (int width : widths) {
				if (width > 0) {
					pSum += width;
				} else {
					nSum += width;
				}
				pSum += hgap;
			}
			if (pSum > 0) {
				pSum -= hgap;
			}
			if (nSum != 0) {
				for (int i = 0; i < widths.length; i++) {
					if (widths[i] < 0) {
						widths[i] = (int) (1.0 * (netParentWidth - pSum) * widths[i] / nSum);
						if (widths[i] < 0) {
							widths[i] = 0;
						}
					}
				}
			}
			int x = insets.left;
			boolean firstColumn = true;
			for (int i = 0; i < widths.length; i++) {
				if (firstColumn) {
					firstColumn = false;
				} else {
					x += hgap;
				}
				List<Component> column = columns[i];
				if (column != null && widths[i] > 0) {
					int y = insets.top;
					boolean firstComponent = true;
					for (Component comp : column) {
						if (!comp.isVisible()) {
							continue;
						}
						if (firstComponent) {
							firstComponent = false;
						} else {
							y += vgap;
							if (grid > 0) {
								y += grid - (y - insets.top) % grid;
							}
						}
						final int height = comp.getPreferredSize().height;
						super.setBounds(comp, x, y, widths[i], height);
						y += height;
					}
				}
				x += widths[i];
			}
		}
	}

	interface Tile {

		int getD1(Component comp);

		int getD2(Component comp);
	}

	static int[] getSize(Container parent, int[] sizes, List<Component>[] allComps, int gap1, int gap2, int grid, Tile tile) {
		int d1 = 0;
		int d2 = 0;
		for (int i = 0; i < sizes.length; i++) {
			if (sizes[i] > 0) {
				d1 += sizes[i];
			} else {
				List<Component> comps = allComps[i];
				if (comps != null) {
					int d = 0;
					for (Component comp : comps) {
						if (comp.isVisible()) {
							d = Math.max(tile.getD1(comp), d);
						}
					}
					d1 += d;
				}
			}
			d1 += gap1;
		}
		if (d1 > 0) {
			d1 -= gap1;
		}
		for (int i = 0; i < allComps.length; i++) {
			List<Component> comps = allComps[i];
			if (comps != null) {
				int d = 0;
				boolean firstComp = true;
				for (Component comp : comps) {
					if (comp.isVisible()) {
						if(firstComp) {
							firstComp = false;
						} else {
							d += gap2;
							if(grid > 0) {
								d += grid - d % grid;
							}
						}
						d += tile.getD2(comp);
					}
				}
				d2 = Math.max(d2, d);
			}
		}
		return new int[]{d1, d2};
	}
}
