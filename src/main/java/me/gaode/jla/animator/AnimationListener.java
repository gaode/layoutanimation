package me.gaode.jla.animator;

/**
 *
 * @author gaode
 */
public interface AnimationListener {

	public void onEvent(AnimationEvent evt);
}
