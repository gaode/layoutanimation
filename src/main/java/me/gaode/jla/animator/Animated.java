package me.gaode.jla.animator;

/**
 *
 * @author gaode
 */
public interface Animated {
	public static final String TOP = "TOP";
	public static final String LEFT = "LEFT";
	public static final String BOTTOM = "BOTTOM";
	public static final String RIGHT = "RIGHT";
	public static final String CENTER = "CENTER";
	public static final String PUSH = "PUSH";
	public static final String OVERLAP = "OVERLAP";

	public void addAnimationListener(AnimationListener listener);
	public boolean removeAnimationListener(AnimationListener listener);
    public void setSkipAnimation(boolean skipAnimation);
    public boolean isSkipAnimation();
}
