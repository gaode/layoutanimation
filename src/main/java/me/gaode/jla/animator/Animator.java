package me.gaode.jla.animator;

import java.awt.Component;
import java.awt.Container;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import javax.swing.Timer;
import me.gaode.jla.animator.AnimationEvent.Type;

/**
 *
 * @author gaode
 */
public class Animator {

    private final Timer timer;
    private Map<Component, ReboundOperation> rebounds;

    Animator(final Container parent, final AnimatedSupport listeners) {
        this.rebounds = Collections.synchronizedMap(new WeakHashMap<Component, ReboundOperation>());
        this.timer = new Timer(DELAY, new ActionListener(){
            private java.util.List<ReboundOperation> ops = new ArrayList<ReboundOperation>();

            @Override
            public void actionPerformed(ActionEvent e) {
                if (rebounds.isEmpty()) {
                    timer.stop();
                    return;
                }
				listeners.fireEvent(new AnimationEvent(Type.BEFORE_FRAME, parent, null));
                ops.clear();
                ops.addAll(rebounds.values());
                for (ReboundOperation op : ops) {
                    if (!op.doRebound()) {
                        rebounds.remove(op.c);
                        listeners.fireEvent(new AnimationEvent(Type.FINAL_STATE, parent, op.c));
                    }
                }
				listeners.fireEvent(new AnimationEvent(Type.AFTER_FRAME, parent, null));
				if(rebounds.isEmpty()) {
					listeners.fireEvent(new AnimationEvent(Type.AFTER_ANIMATION, parent, null));
                       timer.stop();
				}
            }
        });
    }
    
    public boolean isInAnimation() {
        return !rebounds.isEmpty();
    }
    
    public Rectangle getDestination(Component c) {
        final ReboundOperation ro = rebounds.get(c);
        if(ro == null) {
            return null;
        }
        return ro.dest;
    }

    void rebound(Component c, int x, int y, int width, int height) {
        final Rectangle b = c.getBounds();
		if(b.x == x && b.y == y && b.width == width && b.height == height) {
			return;
		}
        final ReboundOperation ro = rebounds.get(c);
        if(ro == null) {
            rebounds.put(c, new ReboundOperation(c, new Rectangle(x, y, width, height)));
        } else {
            ro.dest.setBounds(x, y, width, height);
        }
        if(!timer.isRunning()) {
            timer.restart();
        }
    }
    private static final int DELAY = 15;
    private static final double FACTOR = 10;
    private static final double VMAX = 20;
    private static final double VMIN = 1;
    private static final double RMAX = 20;
    private static final double RMIN = 1;

	void removeComponent(Component comp) {
		this.rebounds.remove(comp);
	}

    private class ReboundOperation {

        private Component c;
        private Rectangle dest;
        private Rectangle last;
        private double x;
        private double y;
        private double w;
        private double h;

        private ReboundOperation(Component c, Rectangle dest) {
            this.c = c;
            this.dest = dest;
            this.last = new Rectangle(c.getBounds());
            this.x = last.x;
            this.y = last.y;
            this.w = last.width;
            this.h = last.height;
        }

        private boolean doRebound() {
            final Rectangle b = c.getBounds();
            if (!last.equals(b)) {
                x = b.x;
                y = b.y;
                w = b.width;
                h = b.height;
            }
            final double dx = dest.x - x;
            final double dy = dest.y - y;
            double fdx = dx / FACTOR;
            double fdy = dy / FACTOR;
			if(fdx < 0 && fdx > -VMIN) {
				fdx = -VMIN;
			} else if(fdx > 0 && fdx < VMIN) {
				fdx = VMIN;
			}
			if(fdy < 0 && fdy > -VMIN) {
				fdy = -VMIN;
			} else if(fdy > 0 && fdy < VMIN) {
				fdy = VMIN;
			}
            final double v = Math.sqrt(fdx * fdx + fdy * fdy);
            if (v > VMAX) {
				x += fdx * VMAX / v;
				y += fdy * VMAX / v;
            } else {
                x += fdx;
                y += fdy;
            }
			if(dx > 0 && x > dest.x || dx < 0 && x < dest.x) {
				x = dest.x;
			}
			if(dy > 0 && y > dest.y || dy < 0 && y < dest.y) {
				y = dest.y;
			}
            final double dw = dest.width - w;
            final double dh = dest.height - h;
			double fdw = dw / FACTOR;
			double fdh = dh / FACTOR;
			if(fdw < 0 && fdw > - RMIN) {
				fdw = -RMIN;
			} else if(fdw > 0 && fdw < RMIN) {
				fdw = RMIN;
			}
			if(fdh < 0 && fdh > -RMIN) {
				fdh = -RMIN;
			} else if(fdh > 0 && fdh < RMIN) {
				fdh = RMIN;
			}
			final double r = Math.sqrt(fdw * fdw + fdh * fdh);
			if(r > RMAX) {
				w += fdw * RMAX / r;
				h += fdh * RMAX / r;
			} else {
				w += fdw;
				h += fdh;
			}
			if(dw > 0 && w > dest.width || dw < 0 && w < dest.width) {
				w = dest.width;
			}
			if(dh > 0 && h > dest.height || dh < 0 && h < dest.height) {
				h = dest.height;
			}
            last.setBounds((int) x, (int) y, (int) w, (int) h);
//			System.out.println(x + "\t" + y + "\t" + w + "\t" + h);
            if (last.equals(dest)) {
                c.setBounds(dest);
                return false;
            } else {
                c.setBounds(last);
                return true;
            }
        }
    }
}
