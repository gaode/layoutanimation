package me.gaode.jla.animator;

import java.awt.Component;
import java.awt.Container;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gaode
 */
public abstract class AnimatedSupport implements Animated {

    private List<AnimationListener> listeners;
    private boolean skipAnimation = false;
	protected boolean skipResize = false;
	protected Animator animator;

	@Override
	public synchronized void addAnimationListener(AnimationListener listener) {
		if (listeners == null) {
			listeners = new ArrayList<AnimationListener>();
		}
		listeners.add(listener);
	}

	@Override
	public synchronized boolean removeAnimationListener(AnimationListener listener) {
		if (listeners != null) {
            return listeners.remove(listener);
		}
		return false;
	}
    
	@Override
    public void setSkipAnimation(boolean skipAnimation) {
        this.skipAnimation = skipAnimation;
    }

	@Override
    public boolean isSkipAnimation() {
        return skipAnimation;
    }

	protected boolean hasListener() {
		return listeners != null;
	}

	protected synchronized void fireEvent(AnimationEvent evt) {
		if (listeners == null) {
			return;
		}
        try {
            for (AnimationListener listener : listeners) {
                listener.onEvent(evt);
            }
        } catch (Throwable t) {
            Logger.getLogger("Animator").log(Level.WARNING, t.getMessage(), t);
        }
	}

	protected void startAnimation(Container target) {
		if (animator == null) {
			animator = new Animator(target, this);
		}
        if(!animator.isInAnimation()) {
            fireEvent(new AnimationEvent(AnimationEvent.Type.BEFORE_ANIMATION, target, null));
        }
	}
    
    protected void setBounds(Component comp, int x, int y, int width, int height) {
        if(skipAnimation || !comp.isVisible()) {
            comp.setBounds(x, y, width, height);
        } else {
			if(skipResize || (comp.getWidth() == 0 && comp.getHeight() ==0)) {
				comp.setSize(width, height);
			}
			if(animator == null) {
				startAnimation(comp.getParent());
			}
			animator.rebound(comp, x, y, width, height);
        }
    }
	
	protected void removeComponent(Component comp) {
		animator.removeComponent(comp);
	}
}
