package me.gaode.jla.animator;

import java.awt.Component;
import java.awt.Container;

/**
 *
 * @author gaode
 */
public class AnimationEvent {

    private final Container parent;
    private final Component child;
    private Type type;

    public AnimationEvent(Type type, Container parent, Component child) {
        this.type = type;
        this.parent = parent;
        this.child = child;
    }

    public Container getParent() {
        return parent;
    }

    public Component getChild() {
        return child;
    }

    public Type getType() {
        return type;
    }

    public enum Type {

        /**
         * Before the animation triggered of this Container
         */
        BEFORE_ANIMATION,
        /**
         * After the animation triggered of this Container. No more component
         * need to move
         */
        AFTER_ANIMATION,
        /**
         * Before each animation frame start
         */
        BEFORE_FRAME,
        /**
         * After each animation frame finished
         */
        AFTER_FRAME,
        /**
         * The animation of child component has reached its final state.
         */
        FINAL_STATE
    }
}
