/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.gaode.jla;

import me.gaode.jla.javax.AFlowLayout;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author gaode
 */
public class Util {

	static Random rnd = new Random();

	public static void main(String... args) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(new Dimension(800, 600));
		frame.setVisible(true);
		Component comp = new SlideLayoutDemo();
		frame.getContentPane().add(comp);
		comp.doLayout();
        comp.repaint();
	}

	public static Color rndColor() {
		return new Color(rnd.nextInt(150), rnd.nextInt(200), rnd.nextInt(250));
	}

	public static JButton createButton(String name, int w, int h) {
		final JButton button = new JButton(name);
		button.setOpaque(true);
		button.setHorizontalTextPosition(JButton.CENTER);
		button.setVerticalTextPosition(JButton.CENTER);
		button.setPreferredSize(new Dimension(w, h));
		button.setBackground(rndColor());
		button.setForeground(Color.white);
		button.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				button.setVisible(false);
			}
		});
		return button;
	}

	public static JPanel createPanel(String name, int w, int h) {
		JPanel p = new JPanel();
		final AFlowLayout layout = new AFlowLayout();
		layout.setHgap(0);
		layout.setVgap(0);
		p.setLayout(layout);
		for (int i = 0; i < 16; i++) {
			p.add(createButton(name + i, 40, 40));
		}
		return p;
	}
}
