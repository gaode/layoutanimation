package me.gaode.jla.javax;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.util.Random;
import javax.swing.JButton;
import me.gaode.jla.RowLayout;

/**
 *
 * @author DGao
 */
public class RowLayoutDemo {
    
    private static Random rnd = new Random();

    public static void main(String...args) throws InterruptedException {
        final Container p = TestLayout.frame();
        final RowLayout layout = new RowLayout(50, 90, 60, 80, -2, -1, 70);
		layout.addAnimationListener(new PerformanceDebug());
        layout.setHGap(5);
        layout.setVGap(5);
		layout.setGrid(100);
        p.setLayout(layout);
        for(int i=0;i<7;i++) {
            for(int j=0;j<10;j++) {
                p.add(newButton(i*j), String.valueOf(i));
            }
        }
//        p.revalidate();
//        for(int i=0;i<70;i++) {
//            p.getComponent(i).setVisible(false);
//            p.revalidate();
//            Thread.sleep(100);
//        }
    }
    
    private static JButton newButton(int i) {
        JButton btn = new JButton(String.valueOf(i));
        btn.setPreferredSize(new Dimension(rnd.nextInt(50) + 60, rnd.nextInt(50) + 40));
        btn.setBackground(new Color(rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)));
        return btn;
    }

}
