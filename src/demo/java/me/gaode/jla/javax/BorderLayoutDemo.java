/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.gaode.jla.javax;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import javax.swing.JLabel;
import static me.gaode.jla.javax.TestLayout.*;

/**
 *
 * @author gaode
 */
public class BorderLayoutDemo {
	
	public static void main(String...args) throws InterruptedException {
		final Container p = frame();
		p.setLayout(new ABorderLayout());
		final JLabel l1 = new JLabel("asdf");
		c(l1);
		p.add(l1, BorderLayout.NORTH);
		final JLabel l2 = new JLabel("asdf");
		c(l2);
		p.add(l2, BorderLayout.SOUTH);
		final JLabel l3 = new JLabel("asdf");
		c(l3);
		p.add(l3, BorderLayout.WEST);
		final JLabel l4 = new JLabel("asdf");
		c(l4);
		p.add(l4, BorderLayout.EAST);
		final JLabel l5 = new JLabel("asdf");
		c(l5);
		p.add(l5, BorderLayout.CENTER);
		Thread.sleep(1000);
		p.doLayout();
		Thread.sleep(2000);
		l1.setPreferredSize(new Dimension(100, 100));
		l2.setPreferredSize(new Dimension(100, 100));
		p.doLayout();
		
	}
}
