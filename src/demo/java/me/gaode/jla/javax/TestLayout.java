/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.gaode.jla.javax;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.HeadlessException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/**
 *
 * @author gaode
 */
public class TestLayout {
	
	public static Container frame() throws HeadlessException {
        return frame(800, 600);
	}

	public static Container frame(int width, int height) throws HeadlessException {
		System.setProperty("sun.java2d.opengl","true");
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TestLayout.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(TestLayout.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(TestLayout.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(TestLayout.class.getName()).log(Level.SEVERE, null, ex);
        }
		JFrame f = new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(width, height);
		f.setVisible(true);
        f.setLocationRelativeTo(null);
		final Container p = f.getContentPane();
		return p;
	}

	public static void c(final JLabel l1) {
		l1.setBackground(Color.red);
		l1.setOpaque(true);
		l1.setPreferredSize(new Dimension(100, 30));
		l1.setBorder(BorderFactory.createLineBorder(Color.yellow, 1));
	}

	public static void d(final JLabel l1) {
		l1.setBackground(Color.BLUE);
		l1.setOpaque(true);
		l1.setPreferredSize(new Dimension(100, 30));
		l1.setBorder(BorderFactory.createLineBorder(Color.yellow, 3));
		final Font f = l1.getFont();
		l1.setFont(f.deriveFont(30));
		l1.setHorizontalTextPosition(SwingConstants.CENTER);
	}

}
