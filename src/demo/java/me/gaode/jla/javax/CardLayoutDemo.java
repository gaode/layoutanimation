/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.gaode.jla.javax;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Container;
import static me.gaode.jla.javax.TestLayout.*;

/**
 *
 * @author gaode
 */
public class CardLayoutDemo {
	public static void main(String...args) throws InterruptedException {
		final Container p = frame();
		final ACardLayout layout = new ACardLayout();
		p.setLayout(layout);
		final JLabel l1 = new JLabel("1");
		final JLabel l2 = new JLabel("2");
		final JLabel l3 = new JLabel("3");
		final JLabel l4 = new JLabel("4");
		d(l1);
		d(l2);
        l2.setBackground(Color.red);
		d(l3);
        l3.setBackground(Color.CYAN);
		d(l4);
        l4.setBackground(Color.GRAY);
		p.add(l1, "1");
		p.add(l2, "2");
		p.add(l3, "3");
		p.add(l4, "4");
		p.doLayout();
		Thread.sleep(4000);
		layout.next(p);
		Thread.sleep(2000);
		layout.next(p);
		Thread.sleep(2000);
		layout.next(p);
		Thread.sleep(2000);
		layout.next(p);
		Thread.sleep(2000);
		layout.first(p);
		layout.show(p, "4");
        p.doLayout();
	}
}
