/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.gaode.jla.javax;

import me.gaode.jla.animator.AnimationEvent;
import me.gaode.jla.animator.AnimationListener;

/**
 *
 * @author DGao
 */
public class PerformanceDebug implements AnimationListener{

    private long frameStart;
	private long animationStart;
    private long delaySum;
    private int count;
    
    @Override
    public void onEvent(AnimationEvent evt) {
        switch(evt.getType()) {
            case BEFORE_ANIMATION:
                delaySum = 0;
                count = 0;
				animationStart = System.currentTimeMillis();
                break;
            case BEFORE_FRAME:
                frameStart = System.nanoTime();
                break;
            case AFTER_FRAME:
                delaySum += System.nanoTime() - frameStart;
                count++;
                break;
            case AFTER_ANIMATION:
                if(count > 1) {
					System.out.printf("Frame rate: %1$d\n", (int)(1000.0 * count / (System.currentTimeMillis() - animationStart)));
					System.out.printf("Avg. Frame Time:%1$d\n", (int)(delaySum / 1000.0 / count));
				}
                break;
        }
    }
    
}
