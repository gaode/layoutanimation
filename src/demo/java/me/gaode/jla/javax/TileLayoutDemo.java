/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.gaode.jla.javax;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import me.gaode.jla.TileLayout;
import me.gaode.jla.TileLayout.Tile;

/**
 *
 * @author gaode
 */
public class TileLayoutDemo {
	
	static Random rnd = new Random();

	public static void main(String... args) {
		final Container p = TestLayout.frame();
        if(p instanceof JPanel) {
            ((JPanel)p).setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        }
		final TileLayout layout = new TileLayout(40, 40, 5, 5);
		layout.addAnimationListener(new PerformanceDebug());
		p.setLayout(layout);
		Tile tile = new Tile();
		for (int i = 0; i < 40; i++) {
			tile.colSpan = rnd.nextInt(2) + 1;
			tile.rowSpan = rnd.nextInt(2) + 1;
            final JButton jButton = new JButton(String.valueOf(i));
            jButton.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    jButton.setVisible(false);
//                    p.revalidate();
                }
            });
			p.add(jButton, tile);
		}
//		p.revalidate();
	}
}
