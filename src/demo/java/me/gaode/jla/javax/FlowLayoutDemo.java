/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.gaode.jla.javax;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;

/**
 *
 * @author gaode
 */
public class FlowLayoutDemo {
	
	public static void main(String...args) throws InterruptedException {
		final Container p = TestLayout.frame();
		final AFlowLayout layout = new AFlowLayout(FlowLayout.LEADING);
//		layout.addAnimationListener(new AnimationDebugPrint());
//        layout.addAnimationListener(new PerformanceDebug());
		p.setLayout(layout);
		Random rnd = new Random();
		for(int i=0;i<40;i++) {
			final JButton l = new JButton("" + i);
			l.setOpaque(true);
			l.setHorizontalTextPosition(JLabel.CENTER);
			l.setPreferredSize(new Dimension(100, 30));
			l.setBackground(new Color(rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)));
			l.setForeground(Color.white);
			l.setBorder(BorderFactory.createLineBorder(Color.black, 3));
			l.setVisible(false);
			l.addActionListener(new ActionListener(){

				@Override
				public void actionPerformed(ActionEvent e) {
					if(l.getWidth() == 100) {
						l.setPreferredSize(new Dimension(200, 60));
//						p.revalidate();
					}else if(l.getWidth() == 200) {
						l.setPreferredSize(new Dimension(100, 30));
//						p.revalidate();
					}
				}
			});
			p.add(l);
			p.setComponentZOrder(l, i);
//			p.revalidate();
		}
		for(int i=39;i>=0;i--) {
			p.getComponent(i).setVisible(true);
			Thread.sleep(200);
		}
		Thread.sleep(1000);
		for(int i=0;i<40;i++) {
			final Component c = p.getComponent(i);
			c.setPreferredSize(new Dimension(200, 60));
			p.validate();
			Thread.sleep(200);
		}
	}
}
