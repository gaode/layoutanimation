/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.gaode.jla.javax;

import java.awt.Dimension;
import javax.swing.JLabel;
import java.awt.Container;
import javax.swing.BoxLayout;
import static me.gaode.jla.javax.TestLayout.*;

/**
 *
 * @author gaode
 */
public class BoxLayoutDemo {
	public static void main(String...args) throws InterruptedException {
		final Container p = TestLayout.frame();
		p.setLayout(new ABoxLayout(p, BoxLayout.X_AXIS));
		JLabel l1 = new JLabel("asdf");
		c(l1);
		l1.setMaximumSize(new Dimension(100, 30));
		p.add(l1);
		JLabel l2 = new JLabel("asdf");
		c(l2);
		l2.setMaximumSize(new Dimension(150, 30));
		p.add(l2);
		JLabel l3 = new JLabel("asdf");
		c(l3);
		l3.setMaximumSize(new Dimension(200, 30));
		p.add(l3);
		JLabel l4 = new JLabel("asdf");
		c(l4);
		l4.setMaximumSize(new Dimension(250, 30));
		p.add(l4);
		p.validate();
		Thread.sleep(2000);
		l1.setMaximumSize(new Dimension(250, 40));
//		p.validate();
//		Thread.sleep(1000);
		l2.setMaximumSize(new Dimension(100, 20));
//		p.validate();
//		Thread.sleep(1000);
		l3.setMaximumSize(new Dimension(150, 50));
//		p.validate();
//		Thread.sleep(1000);
		l4.setMaximumSize(new Dimension(200, 60));
		p.validate();
	}
}
