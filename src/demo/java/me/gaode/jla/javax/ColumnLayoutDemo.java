package me.gaode.jla.javax;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.util.Random;
import javax.swing.JButton;
import me.gaode.jla.ColumnLayout;

/**
 *
 * @author DGao
 */
public class ColumnLayoutDemo {
    
    private static Random rnd = new Random();

    public static void main(String...args) throws InterruptedException {
        final Container p = TestLayout.frame();
        final ColumnLayout layout = new ColumnLayout(50, 90, 60, 80, -2, -1, 70);
		layout.addAnimationListener(new PerformanceDebug());
        layout.setHGap(2);
        layout.setVGap(2);
		layout.setGrid(80);
        p.setLayout(layout);
        for(int i=0;i<7;i++) {
            for(int j=0;j<7;j++) {
                p.add(newButton(i*j), String.valueOf(i));
            }
        }
//		layout.setSkipAnimation(true);
//        p.revalidate();
        Thread.sleep(1000);
//        for(int i=0;i<49;i++) {
//            p.getComponent(i).setVisible(false);
//            p.revalidate();
//            Thread.sleep(100);
//        }
//        for(int i=0;i<49;i++) {
//            p.getComponent(i).setVisible(true);
//            p.revalidate();
//            Thread.sleep(600);
//        }
//        for(int i=48;i>-1;i--) {
//            p.getComponent(i).setVisible(true);
//            p.revalidate();
//            Thread.sleep(600);
//        }

    }
    
    private static JButton newButton(int i) {
        JButton btn = new JButton(String.valueOf(i));
        btn.setPreferredSize(new Dimension(rnd.nextInt(50) + 40, rnd.nextInt(50) + 40));
        btn.setBackground(new Color(rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)));
		btn.setOpaque(true);
        return btn;
    }
}
